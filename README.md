## Description

Nest framework TypeScript Blog API.

## Installation

```bash
$ yarn install
```

## Setting Env

```bash
cp .env.example .env
```

Set values to enviroment variables
```
PORT=[YOUR_PORT] #example: 3000

DB_HOST=[YOUR_HOST]
DB_PORT=[YOUR_PORT]
DB_USER=[YOUR_USER]
DB_PASS=[YOUR_PASS]
DB_NAME=[YOUR_NAME]

JWT_SECRET=[A_SECRET_TOKEN]
JWT_EXPIRES_IN="1d" #Time to expire the JWT
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev
```

##### Dev: Eber Reta