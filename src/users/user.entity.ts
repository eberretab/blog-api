import { Post } from "src/posts/post.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
    
    @Column({ unique: true})
    email: string;
    
    @Column()
    password: string;

    @Column({ type : 'datetime', default: () => 'CURRENT_TIMESTAMP'})
    created_at: Date;

    // Relationship with Post
    @OneToMany(() => Post, post => post.author)
    posts: Post[];
}