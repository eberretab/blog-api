import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { User } from 'src/users/user.entity';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private usersRepository: Repository<User>) {}

    async getAllUsers(): Promise<User[]> {
        return this.usersRepository.find();
    }

    async findOne(id: number): Promise<User | undefined> {
        return this.usersRepository.findOne({where: { id } });
    }

    async findByEmail(email: string): Promise<User | undefined> {
        return this.usersRepository.findOne({where: { email } });
    }
}
