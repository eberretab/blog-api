import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';

import { AuthGuard } from 'src/auth/auth.guard';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { User } from './user.entity';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    /**
     * @returns Return a list of all users
     */
    @UseGuards(AuthGuard)
    @Get()
    getUsers(): Promise<User[]> {
        return this.usersService.getAllUsers();
    }
}
