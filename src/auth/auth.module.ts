import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, forwardRef } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { User } from 'src/users/user.entity';
import { AuthService } from 'src/auth/auth.service';
import { UsersModule } from 'src/users/users.module';
import { AuthController } from 'src/auth/auth.controller';

@Module({
  imports: [
    UsersModule, 
    ConfigModule,
    TypeOrmModule.forFeature([User]), forwardRef(() => AuthModule),
    JwtModule.registerAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
            global: true,
            secret: configService.get('JWT_SECRET'),
            signOptions: { expiresIn: configService.get('JWT_EXPIRES_IN') },
        }),
    })
  ],
  providers: [AuthService],
  controllers: [AuthController],
  exports: [AuthModule, JwtModule],
})
export class AuthModule {}
