import { Body, Controller, Post } from '@nestjs/common';

import { User } from 'src/users/user.entity';
import { AuthService } from 'src/auth/auth.service';
import { LoginUserDto } from 'src/users/dto/login-user.dto';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    /**
     * Login a user
     * @returns Return the user and a JWT token
     */
    @Post('login')
    login(@Body() body: LoginUserDto) {
        return this.authService.login(body.email, body.password);
    }


    /**
     * Create a new user
     * @returns Return the created user and a JWT token
     */
    //@UseGuards(AuthGuard)
    @Post('register')
    createUser(@Body() body: CreateUserDto) : Promise<Partial<User> & {access_token: string}> {
        return this.authService.createUser(body);
    }

}