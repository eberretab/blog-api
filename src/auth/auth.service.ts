import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';

import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { User } from 'src/users/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private usersRepository:Repository<User>,
        private jwtService: JwtService, 
        private userService: UsersService
    ) { }

    async login(email: string, password: string) {
        const user = await this.userService.findByEmail(email)

        if (user && await bcrypt.compare(password, user.password)) {
            const { password, ...result } = user;
            const payload = { id: user.id , email: user.email };
            return {
                ...result,
                access_token: this.jwtService.sign(payload),
            };
        }

        return { error: 'Invalid email or password'};
    }

    async createUser(user: CreateUserDto): Promise<Partial<User> & {access_token: string}> {  
        const hash = await bcrypt.hash(user.password, 10);
        user.password = hash;
        const newUser = await this.usersRepository.save(user);
        
        const payload = { id: newUser.id , email: newUser.email };
        const { password, ...result } = newUser;

        return {
            ...result,
            access_token: this.jwtService.sign(payload),
        };

    }
}
