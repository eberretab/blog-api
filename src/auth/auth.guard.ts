import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();   
    const token = this.getBearerToken(request);
    try{
      const payload = await this.jwtService.verifyAsync(token);
      request.user = payload;
      return true;
    } catch (e){
      console.error(e);
      return false;
    }
  }

  private getBearerToken (request: Request) : string {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : "";
  }
}
