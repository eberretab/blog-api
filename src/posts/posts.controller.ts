import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';

import { AuthGuard } from 'src/auth/auth.guard';
import { PostsService } from 'src/posts/posts.service';
import { Post as PostEntity } from 'src/posts/post.entity';
import { CreatePostDto } from 'src/posts/dto/create-post.dto';


@Controller('posts')
export class PostsController {
    constructor(private postsService: PostsService) {}
    
    /**
     * @returns Return a list of all posts
     */
    @Get()
    getPosts(): Promise<PostEntity[]> {
        return this.postsService.getAllPosts(); 
    }

    /**
     * @returns Return a post by id
     */ 
    @Get(':id')
    getPost(@Param('id') id: string): Promise<PostEntity> {
        return this.postsService.getPostById(parseInt(id));   
    }

    /**
     * Create a new post
     * @returns Return the created post
     */
    @UseGuards(AuthGuard)
    @Post()
    createPost(@Body() body: CreatePostDto, @Req() req): Promise<PostEntity> {
        const userId  = req.user.id;
        
        return this.postsService.createPost(body, userId);
    }

    @Get('search/:searchValue')
    searchPosts(@Param('searchValue') searchValue: string): Promise<PostEntity[]> {
        return this.postsService.searchPosts(searchValue);
    }
}
