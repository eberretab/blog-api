import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Post } from 'src/posts/post.entity';
import { AuthGuard } from 'src/auth/auth.guard';
import { AuthModule } from 'src/auth/auth.module';
import { PostsService } from 'src/posts/posts.service';
import { PostsController } from 'src/posts/posts.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    TypeOrmModule.forFeature([Post])
  ],
  controllers: [PostsController],
  providers: [AuthGuard, PostsService]
})

export class PostsModule {}