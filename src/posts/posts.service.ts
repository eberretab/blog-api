import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Post } from 'src/posts/post.entity';
import { UsersService } from 'src/users/users.service';
import { CreatePostDto } from 'src/posts/dto/create-post.dto';

@Injectable()
export class PostsService {
    constructor(
        @InjectRepository(Post) private postsRepository: Repository<Post>,
        private usersService: UsersService
    ) {}

    getAllPosts(): Promise<Post[]> {    
        return this.postsRepository.createQueryBuilder('post')
            .select(['post.id as id','title', 'author.name as author_name','post.created_at as created_at'])
            .addSelect(`CASE
                WHEN LENGTH(post.content) <= 70 THEN post.content
                ELSE
                CONCAT(SUBSTRING(post.content, 1, 70),'...') END`, 
            'content')
            .leftJoin('post.author', 'author', 'author.id = post.author')
            .orderBy('post.created_at', 'DESC')
            .getRawMany();
    }   

    getPostById(id: number): Promise<Post> {
        return this.postsRepository.findOne({
            where: { id },
            relations: ['author']
        });

    }

    async createPost(post: CreatePostDto, userId: number): Promise<Post> {
        const user =  await this.usersService.findOne(userId);
        post.author = user.id;

        const postSaved = await this.postsRepository.save({...post});
        return postSaved;
    }

    async searchPosts(searchValue:string) :Promise<Post[]> {    
        return this.postsRepository.createQueryBuilder('post')
            .select(['post.id as id','title', 'author.name as author_name','post.created_at as created_at'])
            .addSelect(`CASE
                WHEN LENGTH(post.content) <= 70 THEN post.content
                ELSE
                CONCAT(SUBSTRING(post.content, 1, 70),'...') END`, 
            'content')
            .leftJoin('post.author', 'author', 'author.id = post.author')
            .where('post.title like :searchValue', {searchValue: `%${searchValue}%`})
            .orWhere('post.content like :searchValue', {searchValue: `%${searchValue}%`})
            .orWhere('author.name like :searchValue', {searchValue: `%${searchValue}%`})
            .orderBy('post.created_at', 'DESC')
            .getRawMany();

    }

}
