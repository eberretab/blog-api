import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

import { User } from "src/users/user.entity";

@Entity()
export class Post{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    title:string;

    
    @Column({type: 'text'})
    content:string;
    
    // Relationship with User
    @ManyToOne(() => User, user => user.posts)
    author: number;
    
    @Column({ type : 'datetime', default: () => 'CURRENT_TIMESTAMP'})
    created_at: Date;


}